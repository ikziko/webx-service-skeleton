/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webx.demoservice.demomodule.entities;

import com.webx.core.base.entities.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author imtiaz
 * @project demo-service
 * @created Wed, March 13, 2022
 */

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(schema = "public")
public class DemoEntity extends BaseEntity {

    @Column(columnDefinition = "varchar(30)", nullable = false)
    private String name;

    @Column(columnDefinition = "boolean default false", nullable = false)
    private Boolean isDefault;

}


