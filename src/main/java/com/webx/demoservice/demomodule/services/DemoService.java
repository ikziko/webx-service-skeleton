package com.webx.demoservice.demomodule.services;


import com.webx.core.base.services.BaseService;
import com.webx.demoservice.demomodule.dtos.DemoDTO;
import com.webx.demoservice.demomodule.entities.DemoEntity;
import com.webx.demoservice.demomodule.repositories.DemoRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author imtiaz
 * @project demo-service
 * @created Wed, March 13, 2022
 */

@Service
@Slf4j
public class DemoService extends BaseService<DemoEntity, DemoDTO> {

    private DemoRepository repository;
    private ModelMapper mapper;

    public DemoService(DemoRepository repository, ModelMapper modelMapper) {
        super(repository, modelMapper);
        this.repository = repository;
        this.mapper = modelMapper;
    }


}
