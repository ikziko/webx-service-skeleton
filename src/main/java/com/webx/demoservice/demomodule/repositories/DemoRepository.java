package com.webx.demoservice.demomodule.repositories;

import com.webx.core.base.repositories.BaseRepository;
import com.webx.demoservice.demomodule.entities.DemoEntity;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author imtiaz
 * @project demo-service
 * @created Wed, March 13, 2022
 */

@Repository
public interface DemoRepository extends BaseRepository<DemoEntity> {


}