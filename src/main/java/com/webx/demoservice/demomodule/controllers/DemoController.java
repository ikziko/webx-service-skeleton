package com.webx.demoservice.demomodule.controllers;


import com.webx.core.base.request.AppRequest;
import com.webx.core.base.response.AppResponse;
import com.webx.demoservice.demomodule.dtos.DemoDTO;
import com.webx.demoservice.demomodule.services.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Set;
import java.util.UUID;

/**
 * @author imtiaz
 * @project demo-service
 * @created Wed, March 13, 2022
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/demo")
public class DemoController {

    @Autowired
    private DemoService service;

    @PostMapping(value = "/get-list")
    public AppResponse getList(@RequestBody AppRequest<DemoDTO> requestDTO) {
        try {
            return service.getList(requestDTO);
        } catch (Exception ex) {
            return AppResponse.build(HttpStatus.INTERNAL_SERVER_ERROR).message(ex.getMessage());
        }
    }

    @GetMapping(value = "/get-by-id/{id}")
    public AppResponse getById(@PathVariable("id") UUID id) {
        try {
            return AppResponse.build(HttpStatus.OK).body(service.get(id));
        } catch (Exception ex) {
            return AppResponse.build(HttpStatus.INTERNAL_SERVER_ERROR).message(ex.getMessage());
        }
    }

    @Transactional
    @PostMapping(value = "/create")
    public AppResponse create(@RequestBody DemoDTO requestDTO) {
        try {
            return AppResponse.build(HttpStatus.INTERNAL_SERVER_ERROR).body(service.create(requestDTO));
        } catch (Exception ex) {
            return AppResponse.build(HttpStatus.INTERNAL_SERVER_ERROR).message(ex.getMessage());
        }
    }

    @Transactional
    @PutMapping(value = "/update")
    public AppResponse updateAll(@RequestBody DemoDTO requestDTO) {
        try {
            return AppResponse.build(HttpStatus.OK).body(service.update(requestDTO));
        } catch (Exception ex) {
            return AppResponse.build(HttpStatus.INTERNAL_SERVER_ERROR).message(ex.getMessage());
        }
    }

    @Transactional
    @PutMapping(value = "/delete-all")
    public AppResponse deleteAll(@RequestBody Set<UUID> requestDTO) {
        try {
            return AppResponse.build(HttpStatus.OK).body(service.delete(requestDTO));
        } catch (Exception ex) {
            return AppResponse.build(HttpStatus.INTERNAL_SERVER_ERROR).message(ex.getMessage());
        }
    }


}
