/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.webx.demoservice.demomodule.dtos;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.webx.core.base.dtos.BaseDTO;
import com.webx.core.base.validators.annotations.ValidEntityOid;
import com.webx.demoservice.demomodule.entities.DemoEntity;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * @author imtiaz
 * @project demo-service
 * @created Wed, March 13, 2022
 */

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DemoDTO extends BaseDTO {

    @ValidEntityOid(DemoEntity.class)
    private UUID id;

    @NotNull(message = "Shop name must be provided")
    private String name;

    private Boolean isDefault;

}


