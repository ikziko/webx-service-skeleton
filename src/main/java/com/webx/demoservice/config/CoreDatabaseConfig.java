package com.webx.demoservice.config;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.webx.core.base.constants.ENV;
import com.webx.core.base.util.EnvConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

/**
 * @author Sarker
 */
@Configuration
public class CoreDatabaseConfig {


    @Bean(name = "coreDbSource")
    @Primary
    public DataSource coreDbSource() {
        HikariConfig config = new HikariConfig();

        config.setUsername(EnvConfig.getString(ENV.DB.CORE.USERNAME, "postgres"));
        config.setPassword(EnvConfig.getString(ENV.DB.CORE.PASSWORD, "postgres"));
        config.setJdbcUrl(EnvConfig.getString(ENV.DB.CORE.JDBC_URL, "jdbc:postgresql://localhost:5432/webx_db?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false"));
        config.setDriverClassName(EnvConfig.getString(ENV.DB.CORE.DRIVER, "org.postgresql.Driver"));

        config.setConnectionTimeout(EnvConfig.getLong(ENV.CONNECTION_TIMEOUT, 600000));
        config.setMaximumPoolSize(EnvConfig.getInt(ENV.CONNECTION_MAX_POOL_SIZE, 500));
        config.setMinimumIdle(EnvConfig.getInt(ENV.CONNECTION_MIN_IDLE_TIME, 20));

        return new HikariDataSource(config);
    }

    @Bean(name = "coreDbTemplate")
    public JdbcTemplate db1Template(@Qualifier("coreDbSource") DataSource mainDataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(mainDataSource);
        return jdbcTemplate;
    }

    @Bean(name = "coreDbNamedTemplate")
    public NamedParameterJdbcTemplate db1NamedParameterJdbcTemplate(@Qualifier("coreDbSource") DataSource mainDataSource) {
        NamedParameterJdbcTemplate jdbcTemplate = new NamedParameterJdbcTemplate(mainDataSource);
        return jdbcTemplate;
    }

}
