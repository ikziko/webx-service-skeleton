package com.webx.demoservice.webclient;

import com.webx.core.base.dtos.LoggedInUserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author imtiaz
 * @project demo-service
 * @created Mon, March 14, 2022
 */
@Slf4j
@Service
public class FeignDataClient {

    @Autowired
    LoggedInUserDTO loggedInUser;

    @Autowired
    AdminServiceClient adminServiceClient;

//    public List<RoleShortDTO> getRoleByIds(Set<UUID> uuidSet) {
//        try {
//            FeignResponse<List<RoleShortDTO>> response = adminServiceClient.getRoleByIds(uuidSet, loggedInUser.getToken());
//            return response.getBody();
//        } catch (Exception e) {
//            log.error("Can't fetch data::" + e.getMessage());
//        }
//        return new ArrayList<>();
//    }
//
//    public RoleShortDTO getRoleByRoleCode(String roleCode) {
//        try {
//            FeignResponse<RoleShortDTO> response = adminServiceClient.getRoleByRoleCode(roleCode, loggedInUser.getToken());
//            return response.getBody();
//        } catch (Exception e) {
//            log.error("Can't fetch data::" + e.getMessage());
//        }
//        return null;
//    }

}
