package com.webx.demoservice.webclient;

import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author imtiaz
 * @project demo-service
 * @created Mon, March 14, 2022
 */

@FeignClient(value = "admin-service", url = "${admin-service.url}")
public interface AdminServiceClient {

//    @RequestMapping(method = RequestMethod.GET, value = "/roles/get-roles-by-ids/{ids}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
//    FeignResponse<List<RoleShortDTO>> getRoleByIds(@PathVariable Set<UUID> ids, @RequestHeader("Authorization") String authorization);
//
//    @RequestMapping(method = RequestMethod.GET, value = "/roles/get-by-role-code/{roleCode}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
//    FeignResponse<RoleShortDTO> getRoleByRoleCode(@PathVariable String roleCode, @RequestHeader("Authorization") String authorization);

}